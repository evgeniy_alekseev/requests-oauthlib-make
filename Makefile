all: deb

deb: clean
	git clone https://github.com/requests/requests-oauthlib
	sed -i -E "s/(__version__ ='[0-9\.]*)/\1\.$(BUILD_NUMBER)/g" requests-oauthlib/requests_oauthlib/__init__.py
	fpm -s python -t deb -n python-requests-oauthlib \
	-d 'python >= 2.7' \
	-d 'python-requests' \
	-d 'python-oauthlib' \
	-a all -C . \
	--deb-user 0 \
	--deb-group 0 \
	--prefix /usr \
	--python-install-lib lib/python2.7/dist-packages \
	requests-oauthlib/setup.py

clean:
	rm -f *.deb
	rm -rf requests-oauthlib
